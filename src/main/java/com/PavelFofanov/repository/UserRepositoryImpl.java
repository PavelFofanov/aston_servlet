package com.PavelFofanov.repository;

import com.PavelFofanov.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepositoryImpl implements UserRepository {

   private List<User> users;

    public UserRepositoryImpl(){
        this.users = new ArrayList<>();
        User user = new User("Pavel", "001", 33, User.Role.ADMIN);
        User user1 = new User("Semen", "002", 13, User.Role.USER);

        users.add(user);
        users.add(user1);
    }

    @Override
    public List<User> findAll() {
        return users;
    }

    @Override
    public boolean isExistAdmin(String name, String password) {
        for (User user : users) {
            if (user.getName().equals(name) && user.getPassword().equals(password) && user.getRole().equals(User.Role.ADMIN)) {
                return true;
            }
        }
        return false;
    }
    @Override
    public boolean isExistUser(String name, String password) {
        for (User user : users) {
            if (user.getName().equals(name) && user.getPassword().equals(password) && user.getRole().equals(User.Role.USER)) {
                return true;
            }
        }
        return false;
    }
}
