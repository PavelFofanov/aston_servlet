package com.PavelFofanov.repository;

import com.PavelFofanov.model.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

//    void save(User user);

    boolean isExistAdmin(String name, String password);

    public boolean isExistUser(String name, String password);
}
