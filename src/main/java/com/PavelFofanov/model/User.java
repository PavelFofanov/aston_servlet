package com.PavelFofanov.model;

public class User {

    private String name;
    private String password;

    private Integer age;

    private Role role;

    public enum Role{
        USER, ADMIN
    }

    public User(String name, String password, Integer age, Role role) {
        this.name = name;
        this.password = password;
        this.age = age;
        this.role = role;
        }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }
}
