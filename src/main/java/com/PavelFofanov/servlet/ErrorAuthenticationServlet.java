package com.PavelFofanov.servlet;

import com.PavelFofanov.repository.UserRepository;
import com.PavelFofanov.repository.UserRepositoryImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.io.IOException;

@WebServlet("/login_error")
public class ErrorAuthenticationServlet extends HttpServlet {
    UserRepository userRepository = new UserRepositoryImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getServletContext().getRequestDispatcher("/jsp/login_error.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String password = req.getParameter("password");

        if (userRepository.isExistAdmin(name, password)) {
            HttpSession session = req.getSession();
            session.setAttribute("user", name);
            resp.sendRedirect(req.getContextPath() + "/home_admin");
        } else if (userRepository.isExistUser(name, password)) {
            HttpSession session = req.getSession();
            session.setAttribute("user", name);
            resp.sendRedirect(req.getContextPath() + "/home_user");
        } else {
            resp.sendRedirect(req.getContextPath() + "/login_error");
        }
    }
}
